\documentclass{IEEEmce}

\usepackage[colorlinks,urlcolor=blue,linkcolor=blue,citecolor=blue]{hyperref}

\usepackage{upmath}


\jmonth{10/07}
\publisheddate{10 07 2024}
\currentdate{02 07 2024}

\pubyear{2024}


\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\setcounter{secnumdepth}{0}

\begin{document}

\sptitle{UNIR - Universidad Internacional de La Rioja}  
\editor{Editor: Name, xxxx@email}

\title{Metodología de Implementación de Seguridad para Arquitectura de Aplicaciones Web}

\author{Kevin Santiago Rey Rodríguez}
\affil{UNIR Facultad de Ingeniería y Tecnología}

\author{Julian David Avellaneda}
\affil{UNIR Facultad de Ingeniería y Tecnología}

\author{Josep Tarrega Juan}
\affil{UNIR Facultad de Ingeniería y Tecnología}

\markboth{Facultad de Ingeniería y Tecnología}{Metodología de Implementación de Seguridad para Arquitectura de Aplicaciones Web}

\begin{abstract} The objective of this work is to provide a comprehensive method for the design, implementation, and validation of security in web application architectures using Vue.js, Spring Boot, and MySQL. This methodology addresses various security requirements across multiple dimensions, including multi-factor authentication, authorization, session management, confidentiality, integrity, secure logging, error and exception handling, availability, input validation, and security headers. The proposed approach ensures that security is integrated from the early stages of development, promoting a secure-by-design philosophy and leveraging best practices to create robust, secure web applications.\end{abstract}

\maketitle

\enlargethispage{10pt}

\section{INTRODUCCIÓN}  

En la actualidad, la adopción de servicios tecnológicos por parte de las corporaciones ha experimentado un crecimiento exponencial, optimizando notablemente su eficiencia operativa\cite{AA1}. Sin embargo, esta transición digital también ha incrementado la susceptibilidad a daños causados por vulnerabilidades y ataques, tanto deliberados como accidentales \cite{BB1} \cite{CC1}. Las aplicaciones web, destacadas por su robustez y capacidad de adaptación en el ámbito corporativo, requieren la integración de rigurosas medidas de seguridad desde las etapas iniciales de su desarrollo para mitigar estos riesgos.\\
Aunque existen protocolos establecidos para considerar y mitigar riesgos de seguridad durante el desarrollo, prever todas las vulnerabilidades desde la definición de requisitos es un desafío significativo. Esto subraya la necesidad de adoptar una perspectiva defensiva en la programación y el uso de herramientas tecnológicas, asignando a los profesionales de seguridad informática un rol crucial dentro de los equipos de desarrollo. La complejidad para anticipar fallos, junto con la disponibilidad y precisión de las herramientas, puede comprometer la atención constante a la seguridad.\\

Dentro del proceso de desarrollo, la ingeniería de requisitos de seguridad proporciona marcos utilizados para mitigar fallos conocidos, organizados en dominios específicos que detallan las áreas de aplicación. Estos requisitos permiten adoptar salvaguardas específicas desde etapas tempranas del desarrollo de software, promoviendo iniciativas estructuradas para mitigar riesgos y medir la madurez de la seguridad en el desarrollo.\\

El presente trabajo se enfoca en la metodología de implementación de seguridad para la arquitectura de aplicaciones web utilizando Vue.js, Spring Boot y MySQL. Se destaca la importancia de integrar la seguridad desde la fase de diseño, asegurando que cada desarrollador pueda generar código de alta calidad y seguro. Adoptar una estrategia de planificación minuciosa desde el inicio permite minimizar la incorporación de fallos de diseño, mejorando la calidad del software y reduciendo la dependencia de revisiones correctivas en etapas avanzadas.\\

En el contexto de la transición tecnológica hacia entornos web, Java se mantiene como una tecnología central debido a su robustez y compatibilidad retroactiva, con Spring Framework y especialmente Spring Boot como estándares industriales para el desarrollo de aplicaciones web. En paralelo, Vue.js se posiciona como una herramienta esencial para el desarrollo de interfaces gráficas modernas, mientras que MySQL se utiliza ampliamente para la gestión segura de bases de datos. Esta metodología propone un enfoque híbrido, combinando prácticas de desarrollo seguro con un enfoque defensivo para garantizar la seguridad a través de diversas capas y fases del ciclo de vida del software.\\

Esta integración de metodologías modernas, como el uso de contenedores optimizados para entornos en la nube, facilita una integración continua y un despliegue eficiente. Así, se asegura que las aplicaciones no solo sean funcionalmente robustas sino también seguras frente a diversas amenazas, promoviendo una cultura de seguridad integral donde cada miembro del equipo de desarrollo es consciente de su rol en la protección del software.


\section{ESTADO DEL ARTE} 

La transformación digital es un pilar fundamental para el crecimiento y la competitividad, tanto de empresas como de países. La digitalización se utiliza para crear oportunidades externas y mejorar los procesos internos, obteniendo beneficios significativos tanto para las empresas como para sus usuarios \cite{AA1} \cite{CC1} . Incluso se utiliza como indicador económico que describe la competitividad de un país en los mercados internacionales \cite{BB1}. El apoyo gubernamental a la transformación digital promueve el crecimiento económico directo e indirecto, siendo un objetivo prioritario en muchos países, como lo señala la Comisión Europea\cite{EE1}. Según el reporte de Gartner 2018, la digitalización de los negocios es una prioridad para las empresas en diversos sectores económicos \cite{FF1}.

\subsection{Importancia de la Seguridad}
El desarrollo de aplicaciones web ha transformado la interacción con la tecnología y la operación empresarial en el mundo digital. Factores como la accesibilidad a Internet, la velocidad de conexión y la potencia de los dispositivos han permitido que las aplicaciones web sean más rápidas y potentes. La proliferación de dispositivos móviles ha cambiado drásticamente el acceso a Internet, impulsando el desarrollo de aplicaciones web responsivas compatibles con múltiples dispositivos.\\

Las expectativas de los usuarios en términos de experiencia e interactividad han aumentado, lo que exige aplicaciones intuitivas y seguras. La seguridad en las aplicaciones web se ha vuelto crítica para proteger datos sensibles, prevenir ataques maliciosos, cumplir con normativas legales como el GDPR y mantener la confianza de los usuarios.

\subsection{Principios de Seguridad en el Software}
La seguridad del software es fundamental debido al crecimiento de las amenazas cibernéticas y la dependencia de aplicaciones y sistemas informáticos. Los principios básicos de seguridad incluyen:

\begin{itemize}
    \item \textbf{Confidencialidad}: Protección contra el acceso no autorizado a la información.
    \item \textbf{Integridad}: Garantía de que la información no ha sido alterada de manera no autorizada.
    \item \textbf{Disponibilidad}: Aseguramiento de que la información esté accesible y utilizable cuando sea necesario.
\end{itemize}

Otros principios incluyen el diseño abierto, fallar de forma segura, mínimos privilegios y separación de privilegios. Tambien existen dentro del ciclo de vida de desarrollo seguro de software (Secure Software Development Life Cycle, SSDLC) la posibilidad de integrar la seguridad desde las etapas iniciales del desarrollo. Modelos como Microsoft Security Development Lifecycle (SDL)\cite{PP1} y Touchpoints de Gary McGraw proporcionan prácticas y directrices específicas para garantizar la seguridad del software . El Secure Software Development Framework (SSDF) del NIST es un marco de trabajo que describe prácticas de desarrollo seguro a un alto nivel, aplicables a cualquier implementación de SDLC  \cite{QQ1} \cite{RR1}.


\subsection{Ingeniería de Requisitos de Seguridad}

La ingeniería de requisitos de seguridad es vital en la fase de análisis del desarrollo de software. Proyectos como SAMM de OWASP ofrecen modelos para medir la madurez en la implementación de seguridad y recomiendan el uso de estándares como OWASP ASVS . Definir claramente los requisitos de seguridad desde el inicio permite identificar y mitigar amenazas y vulnerabilidades de manera efectiva\cite{NN1} \cite{MM1} \cite{OO1}.

\subsection{Paradigma Funcional y Seguridad}
El paradigma funcional aplicado a los requisitos no funcionales de seguridad, como la inmutabilidad de datos y las funciones puras, contribuye significativamente a la creación de aplicaciones seguras. La inmutabilidad asegura que los datos no sean alterados, y las funciones puras garantizan el determinismo y la ausencia de efectos secundarios, mejorando la seguridad y la confiabilidad del código\cite{KK1} \cite{LL1}.\\

El objetivo de este artículo es destacar la importancia crucial de integrar prácticas de seguridad en cada fase del ciclo de vida del desarrollo de software. Desde el diseño inicial hasta el despliegue final, es esencial adoptar un enfoque que garantice la creación de aplicaciones web robustas y seguras. Implementar metodologías modernas y enfoques híbridos, como DevSecOps y Secure Software Development Life Cycle (SSDLC), no solo mejora la seguridad del software sino que también fomenta una cultura de seguridad integral dentro del equipo de desarrollo.\\

Asegurar que cada miembro del equipo entienda y asuma su papel en la protección del software es fundamental para mitigar riesgos y vulnerabilidades desde las primeras etapas del desarrollo. Este enfoque preventivo no solo optimiza recursos y reduce costos asociados con la corrección de errores en fases avanzadas, sino que también refuerza la confianza en la calidad y seguridad del producto final. En un entorno digital cada vez más complejo y amenazado por ciberataques, la integración de la seguridad como un componente central del desarrollo de software se presenta no solo como una necesidad técnica sino también como un imperativo estratégico para cualquier organización comprometida con la excelencia y la seguridad.

\section{OBJETIVOS Y METODOLOGÍA}
\subsection{Objetivo general}
Proporcionar un método de diseño, implementación y validación para el desarrollo
seguro de software con un enfoque orientado a la ingeniería  de requisitos atendiendo a
cada una de sus dimensiones. \\
\subsection{Objetivos específicos}
Determinar el modelo de desarrollo seguro a seguir para la elaboración y validación
de lo requisitos de seguridad. \\
Diseñar los requisitos de seguridad atendiendo a cada una de sus dimensiones. \\
Desarrollar una aplicación como prueba de concepto utilizando las tecnologías Vue,
Spring Boot y MySql siguiendo al método de diseño proporcionado. \\
Validar el método proporcinado con herramientas de auditoría. \\

\section{CONTRIBUCIÓN}

\subsection{Modelo de Requerimientos de Seguridad - OWASP}
La identificación de requisitos de seguridad se basa en el modelo OWASP Application Security Verification Standard (ASVS), que proporciona un marco estructurado para garantizar la seguridad de las aplicaciones web. Este modelo cubre varias áreas clave de seguridad, incluyendo:

\begin{itemize}
    \item \textbf{Autenticación}: Implementación de autenticación multifactorial para garantizar que solo los usuarios autorizados puedan acceder al sistema. Se busca asegurar que algo o alguien sea verificado y que este proceso de verificación
    no sea susceptible a algún tipo de personificación o intersección de credenciales.
    \item \textbf{Control de accesos}: Hacer que la aplicación maneje una correcta autorización o acceso de manera controlada y específica para cada uno de los recursos disponibles, basada en roles y permisos, implementando el principio de privilegios mínimos.
    \item \textbf{Gestión de Sesiones}: Garantizar una correcta interacción del usuario y su estado con la aplicación, se busca que esto sea individual y no pueda ser compartido, además de invalidar cuando ya no sea necesario.
    \item \textbf{Validación, sanitización y codificado}: Securizar toda la información que es procesada e introducida en el sistema, haciendo una respectiva validación a nivel de codificado usado, valores, rangos, filtros y sanitizar cualquier valor almacenado
    \item \textbf{Base de datos}: Garantizar que los accesos a los almacenes de datos sea de forma segura, tanto a nivel de configuración, integración con la aplicación y comunicación.
    \item \textbf{Almacenamiento de datos cifrados}: Implementar la aplicación con módulos de criptografía que ayudan a evitar errores no controlados, y que los accesos a cualquier dato sensible se maneje de forma segura. Cifrado de datos tanto en tránsito como en reposo, y uso de ORM y sentencias preparadas para prevenir inyecciones SQL.
    \item \textbf{Registro y auditoria}: La información provista sea la necesaria y adecuada para los interesados, sin exponer o revelar información no necesaria o sensible, así como controlar la cantidad de registros, reduciendo el ruido y haciendo limpiados periódicos. Implementación de logs seguros para el monitoreo y auditoría de actividades.
    \item \textbf{Protección de datos}: Asegurar que la información esté protegida en todas sus dimensiones, es decir que sea confiable, protegida de ser examinada por actores no autorizados, íntegra, que no pueda ser alterada de forma maliciosa. Disponible, que siempre se pueda acceder cuando el usuario la requiera.
    \item \textbf{Comunicación}: Se busca que a nivel de comunicación entre componentes de la aplicación se haga de forma segura, para esto se pretende hacer uso de buenas medidas de cifrado, en conjunto con configuraciones adecuadas y que sigan los últimos estándares dentro de la industria.
    \item \textbf{Seguridad en el código}: Se busca que el código fuente de la aplicación cumpla con requerimientos de seguridad que lo protejan de instrucciones malintencionadas o no deseadas, puertas traseras, o de heredar código malicioso.
    \item \textbf{API y servicios web}: Se busca que la capa de servicio API, en este caso de tipo REST y usando JSON, tenga una autenticación adecuada, un manejo correcto de sesión y autorización, que se apliquen controles efectivos de seguridad y se que validen todos los parámetros de entrada.
    \item \textbf{Configuración}: Se busca que las configuraciones realizadas en el sistema para entornos productivos puedan proteger al mismo contra ataques comunes, asegurando que el sistema al ser puesto de forma online esté seguro.
\end{itemize}
\subsection{Requerimientos de Seguridad}

La definición de los requisitos de seguridad específicos para la aplicación se realiza a partir de la identificación de amenazas potenciales y la evaluación de vulnerabilidades. Esto incluye la elaboración de un documento de especificaciones que detalla los activos a proteger, las amenazas detectadas, las posibles vulnerabilidades, los riesgos analizados y las prácticas recomendadas para mitigar dichos riesgos.

\subsection{Casos de Uso}

Se desarrollan casos de uso específicos para validar los requisitos de seguridad definidos. Algunos de los principales casos de uso incluyen:

\begin{itemize}
    \item \textbf{Generación de Reportes por el Usuario}: Los usuarios pueden generar y acceder a reportes dentro del sistema, asegurándose de que solo aquellos con los permisos adecuados puedan hacerlo.
    \item \textbf{Asignación de Tickets por el Administrador}: Los administradores pueden asignar tickets a los miembros del equipo de soporte, garantizando que solo los administradores tengan este nivel de acceso.
    \item \textbf{Gestión de Usuarios por el Administrador}: Los administradores pueden gestionar cuentas de usuario, incluyendo la creación, modificación y eliminación, asegurando que solo ellos puedan realizar estas acciones.
\end{itemize}

\subsection{Paradigmas Aplicados}

Se aplicaron paradigmas de programación funcional, como la inmutabilidad de datos y las funciones puras, para mejorar la seguridad y la confiabilidad del sistema. Estos principios ayudan a evitar modificaciones no autorizadas de los datos y aseguran un comportamiento determinista del código, lo que es crucial para mantener la integridad y seguridad del software.\\

En resumen, el desarrollo de esta contribución se centró en crear un sistema seguro y robusto, siguiendo metodologías y prácticas de seguridad reconocidas. La implementación de estos principios y herramientas garantiza que la aplicación web no solo cumpla con sus requisitos funcionales, sino que también esté protegida contra una amplia gama de amenazas y vulnerabilidades.


\section{RESULTADOS}

Durante el transcurso del presente proyecto, se pudo evidenciar la importancia de incluir aspectos de seguridad durante cada fase del desarrollo de software 
añadiendo capas de seguridad adicionales las cuales mitigan las posibles vulnerabilidades presentes en el desarrollo, asimismo,  permitió que cada miembro del equipo 
de desarrollo estuviera involucrado. \\

Contar con una serie de requisitos de seguridad facilita tanto el desarrollo como la validación de la aplicación, lo cual tiene ventajas a largo plazo, 
como se pudo demostrar durante la evaluación de la metodología, la mayoría de los resultados fueron positivos. Adicionalmente, se pudo comprobar que aplicar 
técnias de programación moderna contribuye a la seguridad de la aplicación. \\

Cabe destacar que la inclusión de los análisis de vulnerabilidades en los pipelines de CI/CD es una buena práctica debido a que con esta acción
se pretende mantener un análisis constante de las posibles vulnerabilidades sin esperar a tener la aplicación en un entorno productivo. Incluir aspectos de seguridad 
en cada iteración del ciclo de vida de desarrollo de software garantiza un alto nivel de seguridad una vez completada cada nueva funcionalidad desarrollada, 
debido a que con cada iteración del proyecto se van añadiendo capas de seguridad garantizando la calidad del código. \\

\section{AGREDECIMIENTOS}

Agradecimientos especiales al director Juan Ramón Bermejo Higuera por su acompañamiento a lo largo del trabajo, y a Phillip Dargel por su profesionalismo y valiosas discusiones técnicas.

\begin{thebibliography}{00}

\bibitem{AA1}
Dan Bergh Johnsson, Daniel Deogun, and Daniel Sawano. Secure by design. Manning Publications, New York, NY, 2019.

\bibitem{BB1}
Päivi Parviainen, Maarit Tihinen, Jukka Kääriäinen, and Susanna Teppola. Tackling the digitalization challenge: How to benefit from digitalization in practice. International Journal of Information Systems and Project Management, 5:63–77, 01 2017.

\bibitem{CC1} 
Nataliia Bezrukova, Larysa Huk, Hanna Chmil, Liudmyla Verbivska, Olena Komchatnykh, and Yevhen Kozlovskyi. Digitalization as a trend of modern development of the world economy. WSEAS TRANSACTIONS ON ENVIRONMENT AND DE- VELOPMENT, 18:120–129, 01 2022.

\bibitem{DD1} 
Adam Mentsiev, M Engel, A Tsamaev, M Abubakarov, and R Yushaeva. The concept of digitalization and its impact on the modern economy. 03 2020.

\bibitem{EE1} 
European Commission. A europe fit for the digital age. \url{https://commission. europa.eu/strategy-and-policy/priorities-2019-2024/europe-fit- digital-age_en, 2024. Retrieved (April 22, 2024)}.

\bibitem{FF1} 
Laurence Goasduff. Is digital a priority for your industry? \url{https://www.gartner. com/smarterwithgartner/is-digital-a-priority-for-your-industry, 2018. Retrieved (April 22, 2024)}.

\bibitem{GG1} 
Narciso Cerpa and June Verner. Why did your project fail? Communications of the ACM, 52:130–134, 12 2009.


\bibitem{HH1} 
Sakshi SINGH and Suresh KUMAR. THE TIMES OF CYBER ATTACKS, volume 13, page 133. ACTA TECHNICA CORVINIENSIS Bulletin of Engineering, 2020.


\bibitem{II1} Kaul C and D. B. M. K Prasad. Analysis of the cyber attacks over the past decade and impact of them on private sector. International Journal of Computer Trends and Technology, pages 35–38, 2015.


\bibitem{JJ1}  Harjinder Lallie, Lynsay Shepherd, Jason Nurse, Arnau Erola, Gregory Epiphaniou, Carsten Maple, and Xavier Bellekens. Cyber security in the age of covid-19: A time- line and analysis of cybercrime and cyber-attacks during the pandemic. Computers \& Security, 105:102248, 03 2021.

\bibitem{KK1} 
Dan Bergh Johnsson, Daniel Deogun, and Daniel Sawano. Secure by design. Manning Publications, New York, NY, 2019.


\bibitem{LL1} 
Dean Wampler. Functional programming for java developers: Tools for better concurrency, abstraction, and agility. O’Reilly Media, 2011.

\bibitem{MM1}
Murugiah Souppaya, Karen Scarfone, and Donna Dodson. Secure software development framework (SSDF) version 1.1:, Feb 2022.

\bibitem{NN1}
Yusuf Mufti, Mahmood Niazi, Mohammad Alshayeb, and Sajjad Mahmood. A readi- ness model for security requirements engineering. IEEE Access, 6:28611–28631, 2018.

\bibitem{OO1}
P. Salini and S. Kanmani. Survey and analysis on security requirements engineering. Computers \& Electrical Engineering, 38(6):1785–1797, 2012.

\bibitem{PP1}
Joe Jarzombek and Karen Goertzel. Security in the software lifecycle: Making soft- ware development processes—and software produced by them—more secure. DRAFT Version 1.2. Department of Homeland Security, 2006.

\bibitem{QQ1}
Wisdom Umeugo. Secure software development lifecycle: A case for adoption in software smes. International Journal of Advanced Research in Computer Science, 14:5–12, 02 2023.

\bibitem{RR1}
José Manuel Ortega. Desarrollo seguro en ingeniería del software. Aplicaciones se- guras con Android, NodeJS, Python y C++. 02 2020.
\end{thebibliography}

\begin{IEEEbiography}{Kevin Santiago Rey Rodriguez}{\,} trabaja actualmente en Governikus Corporation, Bremen, Alemania, como desarrollador de software. Recibió el título de Ingeniero en Ciencias de la Computación de la Universidad de Bremen en 2021 y el título de Ingeniero de Sonido de la Universidad de San Buenaventura en 2016. Sus intereses de investigación incluyen la arquitectura de software, la programación funcional y el aprendizaje automático. Santiago es competente en React.js, Spring Framework y DevOps, y posee certificaciones en Diseño Orientado a Dominio (DDD) y Administración de Kubernetes. Es un contribuyente activo de proyectos de código abierto con proyectos en Next.js y Nuxt.js disponibles en GitHub.
\end{IEEEbiography}

\begin{IEEEbiography}{Julian David Avellaneda}{\,} trabaja actualmente en Phoenix NAP, Santa Venera, Malta, como miembro del equipo de desarrollo de software.
\end{IEEEbiography}

\begin{IEEEbiography}{Josep Tarrega Juan} {\,} trabaja actualmente en Innovageo software, Xirivella, España, como miembro del equipo de desarrollo de software.
\end{IEEEbiography}


\end{document}