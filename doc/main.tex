\documentclass[12pt, letterpaper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[spanish]{babel}
\usepackage{setspace}
\usepackage{lipsum}
\usepackage{titlesec}
\usepackage[table,xcdraw]{xcolor}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{array}
\usepackage{booktabs}
\usepackage{longtable}
\usepackage{geometry}
\usepackage{pdflscape}
\usepackage{listings}
\usepackage{pdfpages}

\lstdefinestyle{mystyle}{
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
    frame=lines
}

\lstset{style=mystyle}


\geometry{a4paper, margin=1in}
\usepackage{nicematrix}

\usepackage[hyphens,spaces,obeyspaces]{url}
\usepackage{hyperref}
\hypersetup{breaklinks=true}


\renewcommand{\footnotesize}{\fontsize{8pt}{10pt}\selectfont}

\titleformat{\section}{\normalfont\fontsize{26}{31}\bfseries}{\thesection}{2em}{}
\titlespacing{\section}{0pt}{5pt}{30pt}

\makeatletter
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
            {-2.5ex\@plus -1ex \@minus -.25ex}%
            {1.25ex \@plus .25ex}%
            {\normalfont\normalsize\bfseries}}
\renewcommand{\maketitle}{
  \begin{flushleft}
    \@title
  \end{flushleft}
  \begin{flushleft}
    \@author
  \end{flushleft}
}
\makeatother

\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}

\title{
  \Large{Metodología de implementación de seguridad para arquitectura de aplicaciones web usando Vue.js, Spring boot y Mysql}
  \vspace{2cm}
}

\author{
  Kevin Santiago Rey Rodríguez\\
  Julian David Avellaneda\\
  Josep Tarrega Juan
  \vspace{2cm}
   \begin{flushright}
 {\textit{ Director}}
    \normalsize{ \\
    Juan Ramon Bermejo Higuera\\
     \vspace{2cm}
    }
  \vspace{1cm}
 \end{flushright}
   UNIR - Universidad Internacional de La Rioja\\
  Facultad de Ingeniería y Tecnología\\
  Máster en Ingeniería de Software y Sistemas Informáticos\\
}


\begin{document}
\begin{titlepage}
  \maketitle
  \begin{minipage}[t]{0.5\textwidth}
    \vspace{1cm}
    \includegraphics[width=0.75\textwidth]{./doc/imgs/logo.png}
  \end{minipage}
  \begin{minipage}[t]{0.5\textwidth}
    \raggedleft
    \vspace{2.5cm}
    Julio 2024
  \end{minipage}
  \newpage
\end{titlepage}



\begin{spacing}{3}
\section*{Resumen}
\end{spacing}

El objetivo de este trabajo es proporcionar una metodología para la implementación de seguridad en la arquitectura de aplicaciones web utilizando Vue.js, Spring Boot y MySQL. La metodología propuesta abarca el diseño, implementación y validación de medidas de seguridad que cubren diversas dimensiones esenciales como la autenticación multifactor, la autorización, la gestión de sesiones, la confidencialidad, la integridad, el registro seguro, el manejo de errores y excepciones, la disponibilidad, la validación de entradas y las cabeceras de seguridad.
\begin{spacing}{1.5}\end{spacing} \noindent
En el desarrollo de este trabajo, se identifican y documentan los requisitos de seguridad específicos necesarios para proteger las aplicaciones web contra diversas amenazas. Se desarrolla una prueba de concepto que implementa estos requisitos en un entorno controlado, permitiendo la evaluación y validación de la efectividad de las medidas de seguridad adoptadas. Además, se emplean herramientas de análisis estático y dinámico para la validación de dependencias y la detección de vulnerabilidades en el código.
\begin{spacing}{1.5}\end{spacing} \noindent
Este trabajo también analiza la integración de prácticas de DevSecOps para asegurar que los criterios de seguridad sean considerados en cada etapa del ciclo de vida del desarrollo del software. Se destacan las ventajas de adoptar un enfoque híbrido que combine prácticas de código seguro y enfoques defensivos, promoviendo una cultura de seguridad integral dentro del equipo de desarrollo.
\begin{spacing}{1.5}\end{spacing} \noindent
Los resultados obtenidos demuestran que la metodología propuesta es efectiva para mejorar la seguridad de las aplicaciones web, reduciendo la probabilidad de vulnerabilidades y facilitando la detección y corrección temprana de fallos. Las conclusiones del estudio sugieren que la implementación de esta metodología puede ser beneficiosa para organizaciones que buscan fortalecer la seguridad de sus aplicaciones web mediante el uso de tecnologías modernas y prácticas de desarrollo seguras.




\newpage
\begin{spacing}{3}
\section*{Abstract}
\end{spacing}

The objective of this work is to provide a methodology for implementing security in web application architecture using Vue.js, Spring Boot, and MySQL. The proposed methodology covers the design, implementation, and validation of security measures addressing essential dimensions such as multi-factor authentication, authorization, session management, confidentiality, integrity, secure logging, error and exception handling, availability, input validation, and security headers.
\begin{spacing}{1.5}\end{spacing}
\noindent You can find in this work identified and documented, specific security requirements that are needed to protect web applications against various threats. A proof of concept implementing these requirements is developed in a controlled environment, allowing for the evaluation and validation of the effectiveness of the adopted security measures. Additionally, static and dynamic analysis tools are employed for dependency validation and vulnerability detection in the code. 
\begin{spacing}{1.5}\end{spacing} 
\noindent This work also analyses the integration of DevSecOps practices to ensure that security aspects are considered at every stage of the software development lifecycle. The advantages of adopting a hybrid approach that combines secure coding practices and defensive approaches are highlighted, promoting a comprehensive security culture within the development team.
\begin{spacing}{1.5}\end{spacing}
\noindent The results demonstrate that the proposed methodology is effective in improving web application security, reducing the likelihood of vulnerabilities, and facilitating the early detection and correction of faults. The study's conclusions suggest that implementing this methodology can be beneficial for organisations seeking to strengthen the security of their web applications through the use of modern technologies and secure development practices.

\include{./doc/thanks}

\newpage
\tableofcontents
\newpage
\listoftables
\newpage
\listoffigures
\newpage


\include{./doc/org}
\include{./doc/intro}
\include{./doc/stateOfArt}
\include{./doc/relatedWorks}
\include{./doc/obj}
\include{./doc/sre}
\include{./doc/development}
\include{./doc/validation}
\include{./doc/conclusiones}


\newpage
\addcontentsline{toc}{section}{Referencias}
\bibliographystyle{unsrt}
\bibliography{./doc/refs}

\include{./doc/anexo_a}

\end{document}
