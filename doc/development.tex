\subsection{Construcción de prueba de concepto}
\begin{spacing}
    {1.5}
\end{spacing}
Para construir una prueba de concepto, es necesario elegir una aplicación que cumpla con ciertas características específicas. Primero, la aplicación debe contar con diferentes pantallas y permitir la gestión de múltiples usuarios con roles variados. Cada rol debe tener distintos niveles de restricción en cuanto al acceso a la información del sistema.
\begin{spacing}
    {1.5}
\end{spacing}
\noindent Por esta razón, se ha seleccionado una aplicación cuya funcionalidad principal sea un sistema de gestión de tickets. Los casos de uso de esta aplicación deben incluir la capacidad de generar reportes por parte de los usuarios. Estos reportes, una vez creados, serán asignados a miembros del personal de soporte por un administrador, para que puedan ser gestionados adecuadamente.
\begin{spacing}
    {1.5}
\end{spacing}
\begin{itemize}
    \item Diversas pantallas para diferentes funcionalidades.
    \item Gestión de múltiples usuarios con roles específicos.
    \item Niveles de restricción de acceso a la información según el rol del usuario.
    \item Funcionalidad principal de gestión de tickets.
    \item Capacidad para generar y asignar reportes a personal de soporte.
\end{itemize}

\subsubsection{Casos de uso}
\begin{spacing}
    {1.5}
\end{spacing}
En la prueba de concepto, como requisitos funcionales, se identificaron tres grandes casos de uso posibles para la aplicación sugerida, los cuales son los siguientes:

\begin{spacing}
    {1}
\end{spacing}

\begin{itemize}
    \item El sistema debe permitir a un usuario generar un reporte\ref{fig:useCase1}.
    \begin{figure}[htbp]
        \centering
        \includegraphics[scale=0.6]{doc/imgs/useCase1.png}
        \caption{Diagrama de primer caso de uso, usuario genera un reporte.}
        \label{fig:useCase1}
    \end{figure}
    \item El sistema debe permitir al administrador asignar reportes a un miembro del personal de soporte \ref{fig:useCase2}.
    \begin{figure}[htbp]
        \centering
        \includegraphics[scale=0.5]{doc/imgs/useCase2.png}
        \caption{Diagrama de segundo caso de uso, administrador asigna a una persona de soporte a un ticket.}
        \label{fig:useCase2}
    \end{figure}
    \newpage
    \item El sistema debe permitir al administrador gestionar los usuarios del sistema \ref{fig:useCase3}.
    \begin{figure}[htbp]
        \centering
        \includegraphics[scale=0.6]{doc/imgs/useCase3.png}
        \caption{Diagrama del tercer caso de uso, administrador puede gestionar usuarios del sistema.}
        \label{fig:useCase3}
    \end{figure}
\end{itemize}

\newpage
\subsubsection{Arquitectura del sistema}
\begin{spacing}
    {1.5}
\end{spacing}

La arquitectura de la aplicación para la prueba de concepto sobre gestión de tickets consta de cuatro componentes principales \ref{fig:arqDia}. Cada uno de ellos debería trabajar de forma independiente de los demás, con la premisa de apoyar en una tarea específica a la lógica de negocio de la manera más eficiente posible. Esta característica lleva a que nuestro sistema se desarrolle como un sistema distribuido.


\begin{itemize}
    \item \textbf{Interfaz gráfica de usuario}: Desarrollada con Vue.js, proporciona una interfaz interactiva y amigable para los usuarios.
    \item \textbf{Lógica de negocio (Backend)}: Implementada en Spring Boot, gestiona la lógica de la aplicación y maneja las solicitudes del frontend.
    \item \textbf{Componente de persistencia}: Utiliza MySQL para almacenar y gestionar los datos de la aplicación de manera eficiente.
    \item \textbf{Sistema de autenticación y seguridad}: Keycloak se encarga de la autenticación y la gestión de usuarios, proporcionando un entorno seguro para la aplicación.
\end{itemize}



\begin{figure}[htbp]
        \centering
        \includegraphics[scale=0.6]{doc/imgs/compDiag.png}
        \caption{Diagrama de componentes de sistema}
        \label{fig:arqDia}
\end{figure}
\begin{spacing}
    {1.5}
\end{spacing}
\noindent Se utilizó Docker para crear una imagen de cada componente con el fin de conseguir un desacople entre los sistemas y simplicidad a la hora de desplegarlos. Tener imágenes de los componentes brinda seguridad en las diferentes fases del desarrollo, como por ejemplo en la liberación y la fase de pruebas.
\begin{spacing}
    {1.5}
\end{spacing}
\noindent Cada uno de los componentes en la etapa de despliegue hizo uso de un Nginx configurado como un proxy inverso. Este componente tiene la finalidad de enrutar de manera efectiva las peticiones a los distintos componentes que están siendo administrados por Docker. Los proxies inversos tienen la ruta donde están alojados los certificados SSL, los cuales son administrados por Certbot.


\newpage
\subsubsection{Enfoque de seguridad atendiendo a los requisitos}
\begin{spacing}
{1.5}
\end{spacing}

Con base a los requerimientos de seguridad definidos, a continuación, se describe cómo se implementaron. Esta no es una explicación paso a paso de la implementación, ya que
las implementaciones pueden varían y existen múltiples formas de conseguir los mismos resultados, sin embargo, se busca describir decisiones claves en el desarrollo de la
aplicación que ayudan a cumplir los requisitos, especificando por qué cada implementación contribuye a cumplir el requisito.
\begin{spacing}{1.5}\end{spacing}

\noindent Un componente fuerte dentro del diseño de la aplicación es Keycloak, el cual permite proveer autenticación en el sistema de forma sencilla y eficaz,
además, este componente provee múltiples configuraciones adicionales que brinda al sistema de varias características, para esta prueba se configuró el flujo autenticación en modo estándar.
Para integrar este componente en la aplicación es necesario agregar las dependencias de oauth server y oauth client, y configurar la integración con el componente por medio 
de propiedades en el archivo de configuración como se muestra en el siguiente ejemplo \ref{appYml}.
\begin{spacing}{1.5}\end{spacing}
\lstinputlisting[caption=Ejemplo de configuración, label=appYml,language=Java]{doc/examples/app.yml} \begin{spacing}{1.5}\end{spacing}

\noindent Se pueden configurar ciertas reglas en keycloak para poder crear políticas de contraseña que dan cumplimiento al requisito \hyperref[table:sr-aut-01]{SR-AUT-01}. 
Para dar solución al requisito \hyperref[table:sr-aut-02]{SR-AUT-02} se configuró un factor de autenticación múltiple, con un código OTP enviando al correo del usuario.
La aplicación no almacena credenciales de los usuarios, cumpliendo el requisito \hyperref[table:sr-aut-03]{SR-AUT-03}, esta tarea está delegada a keycloak, al recibir
la petición, previamente validada se comprueba el correo, si este no existe se crea el cliente en el sistema, esto se implementa por medio de un Interceptor en la
aplicación Java, como se muestra en el siguiente ejemplo simplificado \ref{interceptor}.
\begin{spacing}{1.5}\end{spacing}
\lstinputlisting[caption=Fragmento simplificado de código, label=interceptor, language=Java]{doc/examples/Interceptor.java} \begin{spacing}{1.5}\end{spacing}

\noindent Otra ventaja que ofrece keycloak es que ayuda a manejar las sesiones en la aplicación, los componentes se pueden integrar con este y usarlo para primeramente
obtener un token de sesión, y adicionalmente validarlo. Cumpliendo el requisito \hyperref[table:sr-sm-01]{SR-SM-01} tanto el frontend como el backend están configurados
para hacer uso de keycloak, cada interacción que el usuario tiene con la aplicación está validada por medio de su sesión. Adicionalmente, atendiendo al requisito
\hyperref[table:sr-sm-02]{SR-SM-02}, se añade una funcionalidad en el frontend donde al cerrar la sesión, esta se invalida, en conjunto con un tiempo de sesión configurado
en keycloak, lo que permite que al hacer uso de un token caducado la sesión sea inválida. La sesión es exclusivamente administrada por medio de este token (requisito \hyperref[table:sr-sm-03]{SR-SM-03})
el cual se almacena de forma segura en la aplicación por medio de una cookie.
\begin{spacing}{1.5}\end{spacing}

\noindent La base de la seguridad en cuanto a los controles de accesos radica en la implementación de roles con el principio de privilegios mínimos, es decir, todo está bloqueado
por defecto y sólo se van configurando los permisos estrictamente necesarios (Requisito \hyperref[table:sr-ac-01]{SR-AC-01}). En la aplicación se han definido los roles admin, soporte y cliente.
En el lado de la interfaz de usuario habilita o deshabilita funcionalidades según este rol.
\begin{spacing}{1.5}\end{spacing}

\noindent En el componente backend las configuraciones de seguridad en la aplicación están centralizadas en una clase para facilitar su implementación, una de sus funciones es extraer los roles del token de sesión,
el cual es un token JWT, este rol luego es usado para controlar el acceso a los recursos expuestos, junto con esta configuración también se verifican los headers de las solicitudes (Requisito \hyperref[table:sr-ws-05]{SR-WS-05}),
donde se comprueba que sea el contenido esperado.
La aplicación adicionalmente está protegida contra CSRF (Requisito \hyperref[table:sr-ac-02]{SR-AC-02}) por diseño, el API es sin estado, es decir la sesión no se administra o almacena en el backend, la aplicación
solo lee el token previamente autorizado que es extraído del autorization header, lo que implica que cada solicitud es autenticada de nuevo. Otra protección es el uso de la cabecera Origin, la cual es validada en cada petición
(Requisito \hyperref[table:sr-ws-04]{SR-WS-04}).
\begin{spacing}{1.5}\end{spacing}

\noindent Las estructuras de datos están definidas y validadas, es decir cada campo tiene restricciones y condiciones que debe cumplir para aceptar algún valor como válido, (Requisito \hyperref[table:sr-vse-01]{SR-VSE-01}), esto se puede 
implementar de diversas formas según el lenguaje, un ejemplo de esto usando Java que restringe la longitud de un campo se muestra a continuación.
\begin{spacing}{1.5}\end{spacing}
\lstinputlisting[caption=Validación de campos, label=Field, language=Java]{doc/examples/Field.java} \begin{spacing}{1.5}\end{spacing}


\noindent En el lado del cliente, el uso de vue como framework provee una capa de protección contra la ejecución de código no deseado,
como puede ser ataques XSS (Requisitos \hyperref[table:sr-vse-02]{SR-VSE-02} y \hyperref[table:sr-vse-03]{SR-VSE-03} ), siguiendo la documentación del framework, tanto el contenido HTML como los atributos de las etiquetas son
escapados.
\begin{spacing}{1.5}\end{spacing}

\noindent El componente de backend está desarrollado usando Spring, el cual provee Spring Data JPA (Requerimiento \hyperref[table:sr-db-01]{SR-DB-01}), por medio del uso de Query Methods,
el cual implementa Java persistence API y a su vez hace de uso de hibernate prepared statements los cuales son una implementación segura contra ataques de inyección de SQL. En el lado de la
base de datos, esta cifra los datos (Requisito \hyperref[table:sr-db-02]{SR-DB-02}), por lo que si son extraídos no pueden ser leídos, adicionalmente la comunicación con la base de datos solo se
permite mediante SSL, esta a su vez está aislada a nivel de red para solo ser accedida por el componente del backend.
\begin{spacing}{1.5}\end{spacing}

\noindent En el cliente, no se almacena ningún tipo de información sensible (\hyperref[table]table:sr-dp-01{SR-DP-01}), es importante identificar la información que usa la aplicación para la correcta
implementación de los lugares para su almacenamiento, la información sensible no debe ser almacenada en el navegador. 
Para la protección de datos, la base de datos está cifrada, por lo que la información almacenada está asegurada,(Requisito \hyperref[table:sr-sc-01]{SR-SC-01}), en el caso de que
esta información sea sensible, se implementó un mecanismo de seguridad adicional (Requisito \hyperref[table:sr-dp-02]{SR-DP-02}), donde se define un tipo de datos especial que busca enmascarar
y proteger su contenido, haciendo que no pueda ser interpretado directamente por la aplicación y solo accedido de forma segura. Como se muestra en el siguiente ejemplo \ref{secret}, el dato Secret
sólo permite hacer uso de su valor sin necesidad de que sea conocido por la aplicación, además de que se enmascara al sobre escribir el método toString(). Este patrón ayuda además
identificar y darles un buen manejo a los datos sensibles, evita que su contenido sea filtrado en cualquier log o excepción.
\begin{spacing}{1.5}\end{spacing}

\lstinputlisting[caption=Implementación de tipo de dato seguro, label=secret, language=Java]{doc/examples/Secret.java} \begin{spacing}{1.5}\end{spacing}

\noindent Atendiendo los requisitos \hyperref[table:sr-sc-03]{SR-SC-03} y \hyperref[table:sr-cb-03]{SR-CB-03}, donde se buscan proteger las cadenas de conexión y credenciales usadas en la aplicación. Se hace uso de un archivo
de propiedades en Spring, el cual centraliza todas las cadenas de conexiones en conjunto con un archivo .env el cual es especificado en el docker compose, por lo que este archivo .env nunca es versionado sólo se suministra cuando se
ejecuta la aplicación. Como se muestra en el siguiente ejemplo \ref{env}, la aplicación está esperando las credenciales para conectarse a la base de datos, en el archivo de docker compose se especifica donde tomar el archivo .env , y este
último contiene los valores que se van a inyectar cuando la aplicación se ejecute, cabe señalar que este archivo .env no debe ser versionado y debe ser almacenado correctamente dependiendo de la plataforma usada para el
despliegue de la aplicación.
\begin{spacing}{1.5}\end{spacing}
\lstinputlisting[caption=Configuración de variables de entorno, label=env, language=Java]{doc/examples/env.yml} \begin{spacing}{1.5}\end{spacing}

\noindent Spring permite configurar el nivel de auditoria por medio de su archivo de configuración \hyperref[table:sr-ehl-01]{SR-EHL-01} , para el control de las excepciones se implementar 
un GlobalExceptionHandler el cual permite de forma centralizada controlar las excepciones en la aplicación y así mismo controlar tanto el código de error http que se devuelve al usuario 
como que traza o mensaje imprime la aplicación, como se muestra en el siguiente ejemplo \ref{ExceptionHandler}. (Requisito \hyperref[table:sr-ehl-02]{SR-EHL-02})
\begin{spacing}{1.5}\end{spacing}
\lstinputlisting[caption=Fragmento de código para manejo de excepciones, label=ExceptionHandler, language=Java]{doc/examples/ExceptionHandler.java} \begin{spacing}{1.5}\end{spacing}

\noindent A nivel de comunicación entre los diferentes componentes todo está cifrado por medio de certificados, habilitando TLS tanto en el lado del cliente (Requisito \hyperref[table:sr-com-01]{SR-COM-01}) como en el lado
del servidor (Requisito \hyperref[table:sr-com-02]{SR-COM-01}). Se usó certbot para habilitar estos protocolos seguros, por medio de TLS 1.3, AES 128 SHA 256.
\begin{spacing}{1.5}\end{spacing}

\noindent Otro aspecto de la programación defensiva es implementar validaciones en todos los datos que maneja la aplicación (Requisito \hyperref[table:sr-cb-02]{SR-CB-02}), como ya se mencionó anteriormente esto se ha implementado de varias formas, creando
estructuras de datos para manejar información sensible o al abstraer funcionalidades del lenguaje aplicándolas al modelo de la aplicación, adicionalmente también todos los campos del model están anotados con sus tipos correspondientes lo que permite
hacer una validación de sus valores, como es habitual en aplicaciones Java, también se usaron DTOs específicos para las respuestas, lo que permite controlar que datos se desean exponer, sin enviar directamente toda la información de la base de datos.
\begin{spacing}{1.5}\end{spacing}

\noindent A nivel de API, en el servicio del lado del servidor, los controladores solo pueden ser usados por usuarios que cuenten con el rol configurado (Requerimiento \hyperref[table:sr-ws-01]{SR-WS-01}),
se implementa expresiones de seguridad con Spring donde se define el rol o roles que debe tener el usuario autenticado para hacer uso del endpoint. Un ejemplo de esta configuración se
muestra a continuación \ref{Controller}, donde solo el cliente con el rol 'user' podría hacer uso de este recurso.
\begin{spacing}{1.5}\end{spacing}
\lstinputlisting[caption=Implementación de tipo de dato seguro, label=Controller, language=Java]{doc/examples/Controller.java} \begin{spacing}{1.5}\end{spacing}

\noindent Otra característica de seguridad en el API, es que las respuestas son mapeadas a objetos específicos (Requisito \hyperref[table:sr-ws-02]{SR-WS-02}). Los cuerpos de las peticiones son validados con los tipos de datos
esperados (Requisito \hyperref[sr-ws-03]{SR-WS-03}).

\noindent Como configuraciones adicionales, la aplicación se despliega por medio de docker, lo que permite definir archivos de configuración que al momento de despliegue pondrán la aplicación en modo producción
(Requisito \hyperref[table:sr-cfg-01]{SR-CFG-01}), las imágenes base utilizadas se ejecutan usando mínimos privilegios, es decir son imágenes que buscan ser seguras de base (Requisito \hyperref[table:sr-cfg-04]{SR-CFG-04}).
Se habilitan las políticas de las cabeceras de seguridad en las configuraciones de Spring (Requisito \hyperref[table:sr-cfg-02]{SR-CFG-02}). En cuanto a la configuración de CORS,
se especifica en el requerimiento \hyperref[table:sr-cfg-03]{SR-CFG-03} este también se configura dentro de las opciones de seguridad de Spring \ref{CorsConfiguration}.
\begin{spacing}{1.5}\end{spacing}
\lstinputlisting[caption=Implementación de CORS, label=CorsConfiguration, language=Java]{doc/examples/CorsConfiguration.java} \begin{spacing}{1.5}\end{spacing}



\subsubsection{Patrón de arquitectura}
Es de gran importancia recalcar la arquitectura del sistema. El proyecto está construido bajo un enfoque DDD (Domain-Driven Design) y Arquitectura Limpia. Esta forma de organización permite que el código se divida en capas lógicas, las cuales agrupan entidades y su comportamiento en un contexto determinado. La estructura por capas contiene tres áreas principales:

\begin{itemize}
\item \textbf{Capa de dominio:} En esta capa se alojan los agregados de nuestro sistema, que encapsulan las entidades y sus comportamientos con los que se interactúa. Esta representación varía en niveles de abstracción según el objetivo específico. Los componentes clave en esta capa incluyen agregados, repositorios y objetos de valor (value objects), que juntos forman un agregado coherente.
\item \textbf{Capa de aplicación:} Esta capa se encarga de alojar los archivos y la lógica de los casos de uso del sistema.
\item \textbf{Capa de infraestructura:} Aquí es donde estará la capa de infraestructura, encargada de la implementación técnica de la comunicación y almacenamiento.
\end{itemize}

\begin{spacing}
    {1.5}
\end{spacing}
\noindent Todos estos componentes se agrupan en una capa conocida como bounded context, la cual da significado y organiza las capas internas de los componentes, como entidades y repositorios entre otros.

\subsubsection{Paradigmas aplicados}

Aunque Java fue concebido como un lenguaje orientado a objetos, en este proyecto se incorporaron conceptos de programación funcional, como la inmutabilidad y las funciones puras, reflejados en la estructura de las clases.
El flujo de datos se diseñó para manejar peticiones y comportamientos como streams, aprovechando las capacidades funcionales del lenguaje. Spring Boot facilita esto mediante la inyección automática de dependencias.
La comunicación entre agregados se realizó a través de interfaces bien definidas, proporcionando una descripción detallada del comportamiento y asegurando una interacción coherente y flexible entre los componentes del sistema.

\subsubsection{DSL en las solicitudes con VUE}

Las peticiones hechas por el componente de Front-end con TypeScript definen un DSL (Domain-Specific Language) para realizar solicitudes HTTP con métodos GET, POST y DELETE a la API protegida del Backend. La DSL se centra en brindar una interfaz fluida y segura para construir estas solicitudes Poniendo en práctica la programación defensiva (Requisito \hyperref[table:sr-cb-01]{SR-CB-01}). Se importa Keycloak para manejar la autenticación, asegurando que todas las solicitudes incluyan un token válido en el encabezado de autorización. Esto brinda la posibilidad de usar el token de manera centralizada en un scope bastante restringido. La URL base del backend se obtiene de una variable de entorno para facilitar la configuración en diferentes entornos.
\begin{spacing}{1.5}
\end{spacing}
\noindent Como se puede ver en el ejemplo \ref{RequestWithPath}, la constante MET define los métodos HTTP permitidos y el tipo METType garantiza que solo estos métodos sean utilizados, proporcionando seguridad de tipos. Es interesante considerar que las peticiones manejan el cuerpo de las solicitudes de manera adecuada al serializarlo a JSON solo cuando es necesario. Además, el uso de TypeScript asegura que cualquier error de tipo sea capturado en tiempo de compilación, reduciendo la posibilidad de errores en tiempo de ejecución. Esto brinda a los programadores una capa de abstracción al uso de la API que pueden utilizar de forma simple, permitiendo que errores de tipado y el uso indebido de datos sensibles, como el token de acceso, sean reducidos.

\lstinputlisting[caption=Implementación de DSL con TS, label=RequestWithPath, language=Java]{doc/examples/RequestWithPath.ts}
\begin{spacing}{1.5}\end{spacing}