@Component
public class UserMapperInterceptor implements HandlerInterceptor {

  @Override
  public boolean preHandle(HttpServletRequest request,
                            HttpServletResponse response,
                            Object handler) throws Exception
  {
      JwtAuthenticationToken authentication =
              (JwtAuthenticationToken) request.getUserPrincipal();
      if (authentication != null)
      {
          Jwt jwt = (Jwt) authentication.getCredentials();
          customerInterfaceImp.find(JWT.getEmailOf(jwt))
                              .mapLeft(throwable -> this.createCustomer(jwt,
                                                                        authentication));
      }
      return true;
  }

}