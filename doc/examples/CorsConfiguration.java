@Bean
public CorsFilter corsFilter()
{
    CorsConfiguration corsConfiguration = new CorsConfiguration();
    corsConfiguration.setAllowCredentials(true);
    corsConfiguration.setAllowedOrigins(Arrays.asList("https://unir-sc.com" ));
    corsConfiguration.setAllowedHeaders(
            Arrays.asList("Origin", "Content-Type", "Accept", "Authorization"));
    corsConfiguration.setAllowedMethods(
            Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));

    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", corsConfiguration);
    return new CorsFilter(source);
}