public final class Secret {
    private final byte[] value;

    public Secret(byte[] value)
    {
        this.value = value;
    }

    @Override
    protected void finalize() throws Throwable
    {
        Arrays.fill(value, (byte) 0);
        super.finalize();
    }

    public <T> T use(Function<? super byte[], ? extends T> f)
    {
        byte[] copy = value.clone();
        try
        {return f.apply(copy);}
        finally {
            Arrays.fill(copy, (byte) 0);
        }
    }

    @Override
    public String toString()
    {
        return "******";
    }
}
