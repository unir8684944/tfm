@ControllerAdvice
@Slf4j
public class ExceptionHandlerConfig {

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        var error = new ErrorResponse("ERROR-Personalizado", "Error in Server");
        log.error(ex.getMessage(), ex);
        return ResponseEntity.badRequest().body(error);
    }
    public record ErrorResponse(String errorCode, String message) {

    }
}