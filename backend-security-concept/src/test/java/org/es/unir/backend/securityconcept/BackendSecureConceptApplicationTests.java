package org.es.unir.backend.securityconcept;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles ("prod")
class BackendSecureConceptApplicationTests {

    @Test
    void contextLoads()
    {
    }

}
