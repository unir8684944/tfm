package org.es.unir.backend.securityconcept.utils;

import org.junit.jupiter.api.Test;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JWTTest {

    @Test
    void getUserNameOf()
    {
        Map<String, Object> claimss = new HashMap<>();
        claimss.put("name",
                    "John Doe");
        Jwt jwt = Jwt.withTokenValue("token")
                     .header("alg",
                             "none")
                     .claims(claims -> claims.putAll(claimss))
                     .build();

        String userName = JWT.getUserNameOf(jwt);

        assertEquals("John Doe",
                     userName);
    }

    @Test
    void getUserPreferNameOf()
    {
        Map<String, Object> claimss = new HashMap<>();
        claimss.put("preferred_username",
                    "jdoe");
        Jwt jwt = Jwt.withTokenValue("token")
                     .header("alg",
                             "none")
                     .claims(claims -> claims.putAll(claimss))
                     .build();

        String userPreferName = JWT.getUserPreferNameOf(jwt);

        assertEquals("jdoe",
                     userPreferName);
    }

    @Test
    void getEmailOf()
    {
        Map<String, Object> claimss = new HashMap<>();
        claimss.put("email",
                    "johndoe@example.com");
        Jwt jwt = Jwt.withTokenValue("token")
                     .header("alg",
                             "none")
                     .claims(claims -> claims.putAll(claimss))
                     .build();

        String email = JWT.getEmailOf(jwt);

        assertEquals("johndoe@example.com",
                     email);
    }

    @Test
    void getRoleTypAdmin()
    {
        Map<String, Object> claimss = new HashMap<>();
        Map<String, Object> realmAccess = new HashMap<>();
        realmAccess.put("roles",
                        Collections.singletonList("admin"));
        claimss.put("realm_access",
                    realmAccess);
        Jwt jwt = Jwt.withTokenValue("token")
                     .header("alg",
                             "none")
                     .claims(claims -> claims.putAll(claimss))
                     .build();

        String role = JWT.getRoleTyp(jwt);

        assertEquals("admin",
                     role);
    }

}
