package org.es.unir.backend.securityconcept.config.interceptors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ExceptionHandlerConfig {

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        var error = new ErrorResponse("ERROR-Personalizado", "Error in Server");
        log.error(ex.getMessage(), ex);
        return ResponseEntity.badRequest().body(error);
    }
    public record ErrorResponse(String errorCode, String message) {

    }
}
