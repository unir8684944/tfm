package org.es.unir.backend.securityconcept.users.application;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.domain.Customer;
import org.es.unir.backend.securityconcept.users.domain.CustomerInterface;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class UserGetAllUseCase {
    CustomerInterface customerInterface;

    public Either<Throwable, List<Customer>> getAllCustomers(String role)
    {
        return role != null ? customerInterface.getAllCustomersByFilter(role)
                            : customerInterface.getAllCustomers();
    }
}
