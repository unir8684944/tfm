package org.es.unir.backend.securityconcept.users.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.utils.JWT;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping ("/api/v1/users")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class MenuUserController {

    String reports = "reports";
    String home = "home";

    @GetMapping ("/menu")
    public ResponseEntity<List<String>> getUser(
            @AuthenticationPrincipal Jwt jwt)
    {
        return switch (JWT.getRoleTyp(jwt))
        {
            case "user" -> ResponseEntity.ok()
                                         .body(List.of(reports,
                                                       "home"));
            case "admin" -> ResponseEntity.ok()
                                          .body(List.of(reports,
                                                        "users",
                                                        home));
            case "support" -> ResponseEntity.ok()
                                            .body(List.of(reports,
                                                          home));
            default -> ResponseEntity.badRequest()
                                     .build();
        };
    }
}
