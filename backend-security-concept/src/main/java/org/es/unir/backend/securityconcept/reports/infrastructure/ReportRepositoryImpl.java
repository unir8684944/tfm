package org.es.unir.backend.securityconcept.reports.infrastructure;

import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.reports.domain.Report;
import org.es.unir.backend.securityconcept.reports.domain.ReportRepository;
import org.es.unir.backend.securityconcept.reports.domain.value_objects.Description;
import org.es.unir.backend.securityconcept.reports.domain.value_objects.Status;
import org.es.unir.backend.securityconcept.reports.domain.value_objects.Title;
import org.es.unir.backend.securityconcept.reports.infrastructure.interfaces.ReportRepositoryJPA;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
@FieldDefaults (level = AccessLevel.PRIVATE, makeFinal = true)
public class ReportRepositoryImpl implements ReportRepository {
    ReportRepositoryJPA reportRepositoryJPA;

    @Override
    public Either<Throwable, Report> createReport(String title,
                                                  String description,
                                                  String reporter)
    {
        return Try.of(() -> reportRepositoryJPA.save(new Report(UUID.randomUUID(),
                                                                new Title(title),
                                                                new Description(description),
                                                                Status.PENDING,
                                                                reporter,
                                                                null)))
                  .toEither();


    }

    @Override
    public Either<Throwable, List<Report>> getAllReport()
    {
        return Try.of(reportRepositoryJPA :: findAll)
                  .toEither();
    }

    @Override
    public Either<Throwable, Report> updateReport(UUID id,
                                                  String support)
    {
        return Try.of(() -> reportRepositoryJPA.findById(id)
                                               .map(report -> new Report(report.getId(),
                                                                         report.getTitle(),
                                                                         report.getDescription(),
                                                                         Status.ASSIGNED,
                                                                         report.getReporter(),
                                                                         support))
                                               .map(reportRepositoryJPA :: save)
                                               .orElseThrow()

                     )
                  .toEither();
    }

    @Override
    public Either<Throwable, List<Report>> getReportAsigned(String supporter)
    {
        return Try.of(() -> reportRepositoryJPA.findBySupportOrReporter(supporter,
                                                                        supporter))
                  .toEither();
    }
}
