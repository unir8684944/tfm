package org.es.unir.backend.securityconcept.users.domain.mappers;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.Secret;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Converter (autoApply = true)
public class SecretsConverter implements AttributeConverter<Secret, String> {
    @Override
    public String convertToDatabaseColumn(Secret secret)
    {
        if (secret == null) {
            return null;
        }
        return secret.use(Arrays :: toString);
    }

    @Override
    public Secret convertToEntityAttribute(String dbData)
    {
        return dbData != null
               ? new Secret(dbData.getBytes(StandardCharsets.UTF_8))
               : null;
    }
}
