package org.es.unir.backend.securityconcept.reports.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.reports.domain.value_objects.Description;
import org.es.unir.backend.securityconcept.reports.domain.value_objects.Status;
import org.es.unir.backend.securityconcept.reports.domain.value_objects.Title;

import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults (level = AccessLevel.PRIVATE)
@Getter
public class Report {
    @Id
    @GeneratedValue (strategy = GenerationType.UUID)
    UUID id;

    @Column (nullable = false) private Title title;

    @Column (nullable = false) private Description description;

    @Column (nullable = false) private Status status;

    @Column (nullable = false) private String reporter;

    String support;
}
