package org.es.unir.backend.securityconcept.users.domain.valueobjects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Entity
@Getter
@FieldDefaults (level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class Roles {
    @Id
    @Column (nullable = false)
    @GeneratedValue (strategy = GenerationType.UUID)
    private UUID uuid;

    private String name;
}
