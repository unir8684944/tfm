package org.es.unir.backend.securityconcept.users.domain.valueobjects;

import jakarta.persistence.Embeddable;

@Embeddable
public record UserEmail(String email) {
    public UserEmail
    {
        if (email == null || email.isEmpty())
        {
            throw new IllegalArgumentException("Email cannot be null or empty");
        }
        if (! email.contains("@"))
        {
            throw new IllegalArgumentException("Email must contain @");
        }
        if (! email.contains("."))
        {
            throw new IllegalArgumentException("Email must contain .");
        }
    }
}
