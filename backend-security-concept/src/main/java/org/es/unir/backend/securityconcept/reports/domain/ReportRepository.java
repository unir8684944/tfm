package org.es.unir.backend.securityconcept.reports.domain;

import io.vavr.control.Either;

import java.util.List;
import java.util.UUID;

public interface ReportRepository {
    public Either<Throwable, Report> createReport(String title,
                                                  String description,
                                                  String reporter);

    public Either<Throwable, List<Report>> getAllReport();

    public Either<Throwable, Report> updateReport(UUID id,
                                                  String support);

    public Either<Throwable, List<Report>> getReportAsigned(String supporter);
}
