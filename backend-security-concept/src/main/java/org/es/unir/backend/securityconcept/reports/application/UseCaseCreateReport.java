package org.es.unir.backend.securityconcept.reports.application;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.reports.domain.Report;
import org.es.unir.backend.securityconcept.reports.infrastructure.ReportRepositoryImpl;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class UseCaseCreateReport {
    ReportRepositoryImpl reportRepository;

    public Either<Throwable, Report> creareReport(String title,
                                                  String description,
                                                  String reportet)
    {
        return reportRepository.createReport(title,
                                             description,
                                             reportet);
    }

}
