package org.es.unir.backend.securityconcept.users.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.application.UserGetAllUseCase;
import org.es.unir.backend.securityconcept.users.domain.Customer;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.Roles;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
@RequestMapping ("/api/v1/users")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class GetAllUserController {
    UserGetAllUseCase userGetAllUseCase;


    @GetMapping ("")
    @PreAuthorize ("hasRole('admin')")
    public ResponseEntity<List<CustomerResponse>> getAllUser(
            @RequestParam String role)
    {

        return userGetAllUseCase.getAllCustomers(role.isEmpty() ? null : "ROLE_" + role)
                                .map(this :: createResponseObject)
                                .map(this :: createResponseEntity)
                                .getOrElse(ResponseEntity.badRequest()
                                                         .build());
    }

    private List<CustomerResponse> createResponseObject(List<Customer> customers)
    {
        return customers.stream()
                        .map(CustomerResponse :: getResponseFrom)
                        .toList();
    }

    private ResponseEntity<List<CustomerResponse>> createResponseEntity(List<CustomerResponse> customerResponses)
    {
        return ResponseEntity.ok()
                             .body(customerResponses);
    }

    record CustomerResponse(
            UUID id,
            String firstName,
            String email,
            List<Roles> roles
    ) {
        public static CustomerResponse getResponseFrom(Customer customer)
        {
            return new CustomerResponse(customer.getUuid(),
                                        customer.getName()
                                                .name(),
                                        customer.getEmail()
                                                .email(),
                                        customer.getRoles());
        }
    }
}
