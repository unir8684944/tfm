package org.es.unir.backend.securityconcept.config;

import org.es.unir.backend.securityconcept.config.interceptors.UserMapperInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    private final UserMapperInterceptor userMapperInterceptor;

    @Autowired
    public WebConfig(UserMapperInterceptor userMapperInterceptor)
    {
        this.userMapperInterceptor = userMapperInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(userMapperInterceptor);
    }
}

