package org.es.unir.backend.securityconcept.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.security.web.header.writers.XXssProtectionHeaderWriter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableMethodSecurity
public class SecurityConfig {

    public static final int MAX_AGE_IN_SECONDS = 31536000;
    public static final String POLICY_DIRECTIVES =
            "default-src 'self'; script-src 'self'; object-src 'none';";
    private final KeycloakLogoutHandler keycloakLogoutHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http,
                                                   CorsConfigurationSource corsConfigurationSource) throws Exception
    {
        http.cors(httpSecurityCorsConfigurer -> httpSecurityCorsConfigurer.configurationSource(corsConfigurationSourceLocal()))
            .authorizeHttpRequests(authorize -> authorize.requestMatchers("/h2-console" +
                                                                          "/**",
                                                                          "/login")
                                                         .permitAll()
                                                         .anyRequest()
                                                         .authenticated())
            .oauth2ResourceServer(oauth2 -> oauth2.jwt(jwt -> jwt.jwtAuthenticationConverter(jwtAuthenticationConverter())))
            .oauth2Login(Customizer.withDefaults())
            .headers(headers -> {

                headers.frameOptions(HeadersConfigurer.FrameOptionsConfig :: sameOrigin);
                headers.xssProtection(xss -> xss.headerValue(XXssProtectionHeaderWriter.HeaderValue.ENABLED_MODE_BLOCK));
                headers.contentSecurityPolicy(csp -> csp.policyDirectives(POLICY_DIRECTIVES));
                headers.referrerPolicy(referrer -> referrer.policy(ReferrerPolicyHeaderWriter.ReferrerPolicy.SAME_ORIGIN));
                headers.httpStrictTransportSecurity(hsts -> hsts.includeSubDomains(true)
                                                                .preload(true)
                                                                .maxAgeInSeconds(MAX_AGE_IN_SECONDS));
            })
            .logout(logout -> logout.addLogoutHandler(keycloakLogoutHandler)
                                    .logoutSuccessUrl("/"))
                .cors(Customizer.withDefaults());

        return http.build();
    }

    @Profile ({"prod"})
    @Bean
    public CorsConfigurationSource corsConfigurationSource()
    {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedOrigins(Arrays.asList("https://unir-sc.com", "http://localhost:5173"));
        corsConfiguration.setAllowedHeaders(Arrays.asList("Origin",
                                                          "Content-Type",
                                                          "Accept",
                                                          "Authorization"));
        corsConfiguration.setAllowedMethods(Arrays.asList("GET",
                                                          "POST",
                                                          "PUT",
                                                          "DELETE",
                                                          "OPTIONS"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",
                                         corsConfiguration);
        return source;
    }


    @Profile ("default")
    public CorsConfigurationSource corsConfigurationSourceLocal()
    {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedOrigins(Arrays.asList("https://unir-sc.com",
                                                          "http://localhost:5173/"));
        corsConfiguration.setAllowedHeaders(Arrays.asList("Origin",
                                                          "Content-Type",
                                                          "Accept",
                                                          "Authorization"));
        corsConfiguration.setAllowedMethods(Arrays.asList("GET",
                                                          "POST",
                                                          "PUT",
                                                          "DELETE",
                                                          "OPTIONS"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",
                                         corsConfiguration);
        return source;
    }

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter()
    {
        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(new KeycloakRealmRoleConverter());
        return converter;
    }

    @Bean
    public AuthenticationManager authenticationManager(PasswordEncoder passwordEncoder,
                                                       UserDetailsService userDetailsService)
    {
        DaoAuthenticationProvider authenticationProvider =
                new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);
        return new ProviderManager(authenticationProvider);
    }

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}
