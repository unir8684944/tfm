package org.es.unir.backend.securityconcept.users.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.application.UserUpdateUseCase;
import org.es.unir.backend.securityconcept.users.domain.Customer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping ("/api/v1/users")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class EditUserController {
    UserUpdateUseCase userUpdateUseCase;

    @PatchMapping ("/update/{email}")
    public ResponseEntity<String> createUser(
            @RequestBody Customer customer,
            @PathVariable String email)
    {
        return userUpdateUseCase.edit(email,
                                      customer)
                                .isRight() ? ResponseEntity.ok()
                                                           .build()
                                           : ResponseEntity.badRequest()
                                                           .body("no created");
    }
}
