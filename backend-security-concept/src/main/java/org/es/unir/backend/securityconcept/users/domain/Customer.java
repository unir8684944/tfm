package org.es.unir.backend.securityconcept.users.domain;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.domain.mappers.SecretsConverter;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.Roles;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.Secret;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.UserEmail;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.UserName;

import java.util.List;
import java.util.UUID;

@Entity
@FieldDefaults (level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Getter
@Table (uniqueConstraints = {
        @UniqueConstraint (columnNames = "email"),
        @UniqueConstraint (columnNames = "name")})
public class Customer{

    @Id
    @Column (nullable = false)
    //@GeneratedValue(strategy = GenerationType.UUID)
    private UUID uuid;

    @Column (unique = true)
    @Embedded
    private UserName name;

    @Column (unique = true)
    @Embedded
    private UserEmail email;

    @Convert (converter = SecretsConverter.class)
    private Secret surname;

    @OneToMany (cascade = CascadeType.ALL, orphanRemoval = true) private List<Roles>
            roles;


    protected Customer()
    {}
}
