package org.es.unir.backend.securityconcept.users.infrastructure.persistence;

import org.es.unir.backend.securityconcept.users.domain.Customer;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.UserEmail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CustomerInterfaceJPA extends JpaRepository<Customer, UUID> {
    Optional<Customer> findCustomerByEmail(UserEmail email);

    List<Customer> findByRoles_Name(String roleName);
}
