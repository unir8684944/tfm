package org.es.unir.backend.securityconcept.users.infrastructure.controllers;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping ("healthCheck")
@RestController
public class HealthCheck {
    @GetMapping
    public ResponseEntity<HealthCheckResponse> getHealthCheck(
            @CurrentSecurityContext SecurityContext context)
    {
        return ResponseEntity.ok()
                             .contentType(MediaType.APPLICATION_JSON)
                             .body(new HealthCheckResponse(context.getAuthentication()
                                                                  .getName()));
    }

    record HealthCheckResponse(String status) {

    }
}
