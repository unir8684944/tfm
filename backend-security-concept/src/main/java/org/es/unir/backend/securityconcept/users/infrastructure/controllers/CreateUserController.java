package org.es.unir.backend.securityconcept.users.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.application.UserCreationUseCase;
import org.es.unir.backend.securityconcept.users.domain.Customer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping ("/api/v1/users")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class CreateUserController {

    UserCreationUseCase userCreationUseCase;


    @PostMapping ("/create")
    public ResponseEntity<String> createUser(
            @RequestBody Customer customer)
    {
        return userCreationUseCase.createCustomer(customer)
                                  .isRight() ? ResponseEntity.ok()
                                                             .build()
                                             : ResponseEntity.badRequest()
                                                             .body("no created");
    }
}
