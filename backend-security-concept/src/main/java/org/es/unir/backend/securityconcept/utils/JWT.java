package org.es.unir.backend.securityconcept.utils;

import lombok.experimental.UtilityClass;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

@UtilityClass
public class JWT {

    public static final String ROLES = "roles";
    public static final String NAME = "name";
    public static final String ADMIN = "admin";
    public static final String SUPPORT = "support";
    public static final String USER = "user";
    public static final String EMAIL = "email";

    public String getUserNameOf(Jwt jwt)
    {
        return (String) getClaimByFun(jwt,
                                      stringObjectMap -> stringObjectMap.get(NAME));
    }

    public String getUserPreferNameOf(Jwt jwt)
    {
        return (String) getClaimByFun(jwt,
                                      stringObjectMap -> stringObjectMap.get(
                                              "preferred_username"));
    }

    public String getEmailOf(Jwt jwt)
    {
        return (String) getClaimByFun(jwt,
                                      stringObjectMap -> stringObjectMap.get(EMAIL));
    }

    public String getRoleTyp(Jwt jwt)
    {
        var realmAccess = (Map<String, Object>) getClaimByFun(jwt,
                                                              stringObjectMap -> stringObjectMap.get("realm_access"));
        var roles = (List<String>) realmAccess.get(ROLES);
        if (roles.contains(ADMIN))
        {
            return ADMIN;
        }
        return roles.contains(SUPPORT) ? SUPPORT : USER;
    }

    private Object getClaimByFun(Jwt jwt,
                                 Function<Map<String, Object>, Object> fun)
    {
        return fun.apply(jwt.getClaims());
    }
}
