package org.es.unir.backend.securityconcept.reports.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.reports.application.UseCaseGetAllReport;
import org.es.unir.backend.securityconcept.utils.JWT;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Stream;

@RestController
@RequiredArgsConstructor
@RequestMapping ("/api/v1/reports")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class GetAllReportsController {
    UseCaseGetAllReport useCaseGetAllReport;

    @GetMapping ()
    public ResponseEntity<List<ReportResponseDTO>> getAllReports(
            @AuthenticationPrincipal Jwt jwt)
    {
        return useCaseGetAllReport.getAllReports(JWT.getRoleTyp(jwt)
                                                    .equals("admin") ? ""
                                                                     :
                                                 JWT.getEmailOf(jwt))
                                  .map(reports -> reports.stream()
                                                         .map(ReportResponseDTO :: createResponse))
                                  .map(Stream :: toList)
                                  .map(reports -> ResponseEntity.ok()
                                                                .body(reports))
                                  .getOrElse(ResponseEntity.badRequest()
                                                           .build());
    }
}
