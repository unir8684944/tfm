package org.es.unir.backend.securityconcept.reports.application;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.reports.domain.Report;
import org.es.unir.backend.securityconcept.reports.infrastructure.ReportRepositoryImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class UseCaseGetAllReport {
    ReportRepositoryImpl reportRepository;

    public Either<Throwable, List<Report>> getAllReports(String id)
    {
        return id.isEmpty() ? reportRepository.getAllReport()
                            : reportRepository.getReportAsigned(id);
    }
}
