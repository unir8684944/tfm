package org.es.unir.backend.securityconcept.reports.infrastructure.interfaces;

import org.es.unir.backend.securityconcept.reports.domain.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ReportRepositoryJPA extends JpaRepository<Report, UUID> {
    public List<Report> findBySupportOrReporter(String support,
                                                String reporter);
}
