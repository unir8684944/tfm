package org.es.unir.backend.securityconcept.reports.application;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.reports.domain.Report;
import org.es.unir.backend.securityconcept.reports.infrastructure.ReportRepositoryImpl;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@FieldDefaults (level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class UseCaseAsignReport {
    ReportRepositoryImpl reportRepository;

    public Either<Throwable, Report> updateReportSupporter(String id,
                                                           String support)
    {
        return reportRepository.updateReport(UUID.fromString(id),
                                             support);
    }
}
