package org.es.unir.backend.securityconcept.users.domain.valueobjects;

import jakarta.persistence.Embeddable;

@Embeddable
public record UserName(String name) {

    public UserName
    {
        if (name.isBlank())
        {
            throw new IllegalArgumentException("Name cannot be blank");
        }
        if (name.length() < 3)
        {
            throw new IllegalArgumentException("Name must be at least 3 characters");
        }
        if (name.length() > 16)
        {
            throw new IllegalArgumentException("Name cannot exceed 16 characters");
        }
    }
}
