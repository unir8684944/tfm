package org.es.unir.backend.securityconcept.config.interceptors;

import io.vavr.control.Either;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.domain.Customer;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.Roles;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.Secret;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.UserEmail;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.UserName;
import org.es.unir.backend.securityconcept.users.infrastructure.interfaces.CustomerInterfaceImp;
import org.es.unir.backend.securityconcept.utils.JWT;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Component
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class UserMapperInterceptor implements HandlerInterceptor {

    CustomerInterfaceImp customerInterfaceImp;

    private static Roles createRoles(GrantedAuthority grantedAuthority)
    {
        return new Roles(UUID.randomUUID(),
                         grantedAuthority.getAuthority());
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception
    {
        JwtAuthenticationToken authentication =
                (JwtAuthenticationToken) request.getUserPrincipal();
        if (authentication != null)
        {
            Jwt jwt = (Jwt) authentication.getCredentials();
            customerInterfaceImp.find(JWT.getEmailOf(jwt))
                                .mapLeft(throwable -> this.createCustomer(jwt,
                                                                          authentication));
        }
        return true;
    }

    private Either<Throwable, Customer> createCustomer(Jwt jwt,
                                                       JwtAuthenticationToken authentication)
    {
        return customerInterfaceImp.create(new Customer(UUID.randomUUID(),
                                                        new UserName(JWT.getUserPreferNameOf(jwt)),
                                                        new UserEmail(JWT.getEmailOf(jwt)),
                                                        new Secret(JWT.getUserPreferNameOf(jwt)
                                                                      .getBytes(StandardCharsets.UTF_8)),
                                                        authentication.getAuthorities()
                                                                      .stream()
                                                                      .map(UserMapperInterceptor :: createRoles)
                                                                      .toList()));
    }
}


