package org.es.unir.backend.securityconcept.users.infrastructure.interfaces;

import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.domain.Customer;
import org.es.unir.backend.securityconcept.users.domain.CustomerInterface;
import org.es.unir.backend.securityconcept.users.domain.valueobjects.UserEmail;
import org.es.unir.backend.securityconcept.users.infrastructure.persistence.CustomerInterfaceJPA;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults (level = AccessLevel.PRIVATE, makeFinal = true)
public class CustomerInterfaceImp implements CustomerInterface {
    CustomerInterfaceJPA customerInterfaceJPA;

    @Override
    public Either<Throwable, Customer> create(Customer customer)
    {
        return Try.of(() -> customerInterfaceJPA.save(customer))
                  .toEither();
    }

    @Override
    public Either delete(String email)
    {
        Customer customer = find(email).get();
        return Try.of(() -> {
                      customerInterfaceJPA.delete(customer);
                      return "deleted";
                  })
                  .toEither();
    }

    @Override
    public Either<Throwable, Customer> find(String email)
    {
        return Try.of(() -> customerInterfaceJPA.findCustomerByEmail(new UserEmail(email))
                                                .get())
                  .toEither();
    }

    @Override
    public Either<Throwable, Customer> update(String email,
                                              Customer customer)
    {
        Customer customerInDb = find(email).get();
        return Try.of(() -> customerInterfaceJPA.save(new Customer(customerInDb.getUuid(),
                                                                   customer.getName(),
                                                                   customer.getEmail(),
                                                                   customer.getSurname(),
                                                                   List.of())))
                  .toEither();

    }

    @Override
    public Either<Throwable, List<Customer>> getAllCustomers()
    {
        return Try.of(customerInterfaceJPA :: findAll)
                  .toEither();
    }

    @Override
    public Either<Throwable, List<Customer>> getAllCustomersByFilter(String filter)
    {
        return Try.of(() -> customerInterfaceJPA.findByRoles_Name(filter))
                  .toEither();
    }


}
