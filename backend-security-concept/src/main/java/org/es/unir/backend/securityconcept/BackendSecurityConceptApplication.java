package org.es.unir.backend.securityconcept;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendSecurityConceptApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(BackendSecurityConceptApplication.class, args);
    }
}
