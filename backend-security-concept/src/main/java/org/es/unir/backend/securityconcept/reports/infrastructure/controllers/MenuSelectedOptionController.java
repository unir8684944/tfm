package org.es.unir.backend.securityconcept.reports.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.utils.JWT;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping ("/api/v1/reports")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class MenuSelectedOptionController {
    @GetMapping ("/select-option")
    public ResponseEntity<List<String>> getUser(
            @AuthenticationPrincipal Jwt jwt)
    {
        return switch (JWT.getRoleTyp(jwt))
        {
            case "user" -> ResponseEntity.ok()
                                         .body(List.of("All",
                                                       "Create"));
            case "admin" -> ResponseEntity.ok()
                                          .body(List.of("All",
                                                        "Newest"));
            case "support" -> ResponseEntity.ok()
                                            .body(List.of("All"));
            default -> ResponseEntity.badRequest()
                                     .build();
        };
    }

}
