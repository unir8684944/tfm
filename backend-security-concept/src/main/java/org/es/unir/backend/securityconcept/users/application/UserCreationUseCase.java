package org.es.unir.backend.securityconcept.users.application;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.domain.Customer;
import org.es.unir.backend.securityconcept.users.domain.CustomerInterface;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class UserCreationUseCase {
    CustomerInterface customerInterface;

    public Either<Throwable, Customer> createCustomer(Customer customer)
    {
        return customerInterface.create(customer);
    }
}
