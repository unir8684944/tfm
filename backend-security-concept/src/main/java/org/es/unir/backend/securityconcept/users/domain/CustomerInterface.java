package org.es.unir.backend.securityconcept.users.domain;

import io.vavr.control.Either;

import java.util.List;

public interface CustomerInterface  {
    public Either<Throwable, Customer> create(Customer customer);

    public Either<Throwable, Customer> delete(String email);

    public Either<Throwable, Customer> find(String email);

    public Either<Throwable, Customer> update(String email,
                                              Customer customer);

    public Either<Throwable, List<Customer>> getAllCustomers();

    public Either<Throwable, List<Customer>> getAllCustomersByFilter(String filter);
}
