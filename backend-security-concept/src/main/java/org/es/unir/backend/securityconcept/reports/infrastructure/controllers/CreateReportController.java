package org.es.unir.backend.securityconcept.reports.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.reports.application.UseCaseCreateReport;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping ("/api/v1/reports")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class CreateReportController {
    UseCaseCreateReport useCaseCreateReport;

    @PostMapping ("")
    @PreAuthorize ("hasRole('user')")
    public ResponseEntity<ReportResponseDTO> createPost(
            @RequestBody ReportRequest reportRequest,
            @AuthenticationPrincipal Jwt jwt)
    {

        return useCaseCreateReport.creareReport(reportRequest.title,
                                                reportRequest.description,
                                                jwt.getClaim("email"))
                                  .map(report -> ResponseEntity.ok()
                                                               .body(ReportResponseDTO.createResponse(report)))
                                  .getOrElse(ResponseEntity.badRequest()
                                                           .build());
    }

    public record ReportRequest(
            String title,
            String description
    ) {}

}
