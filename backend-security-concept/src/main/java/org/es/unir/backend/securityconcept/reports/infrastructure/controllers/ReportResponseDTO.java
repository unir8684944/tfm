package org.es.unir.backend.securityconcept.reports.infrastructure.controllers;

import org.es.unir.backend.securityconcept.reports.domain.Report;

public record ReportResponseDTO(
        String id,
        String title,
        String description,
        String status,
        String reporter,
        String support
) {
    public static ReportResponseDTO createResponse(Report report)
    {
        return new ReportResponseDTO(report.getId()
                                           .toString(),
                                     report.getTitle()
                                           .title(),
                                     report.getDescription()
                                           .description(),
                                     report.getStatus()
                                           .toString(),
                                     report.getReporter(),
                                     report.getSupport());
    }

}
