package org.es.unir.backend.securityconcept.reports.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.reports.application.UseCaseAsignReport;
import org.es.unir.backend.securityconcept.reports.domain.Report;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping ("/api/v1/reports")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class UpdateReportSupporterController {
    UseCaseAsignReport useCaseAsignReport;

    @PostMapping ("/{id}")
    public ResponseEntity<Report> updateUseCase(
            @PathVariable String id,
            @RequestBody UpdateSupporterRequest supporter)
    {
        return useCaseAsignReport.updateReportSupporter(id,
                                                        supporter.supporterId)
                                 .map(customer -> ResponseEntity.ok()
                                                                .body(customer))
                                 .getOrElse(ResponseEntity.badRequest()
                                                          .build());
    }

    private record UpdateSupporterRequest(String supporterId) {

    }
}
