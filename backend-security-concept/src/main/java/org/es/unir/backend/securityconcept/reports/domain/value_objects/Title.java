package org.es.unir.backend.securityconcept.reports.domain.value_objects;

import jakarta.persistence.Embeddable;

@Embeddable
public record Title(String title) {

}
