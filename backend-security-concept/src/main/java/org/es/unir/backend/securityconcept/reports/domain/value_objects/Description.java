package org.es.unir.backend.securityconcept.reports.domain.value_objects;

import jakarta.persistence.Embeddable;

@Embeddable
public record Description(String description) {

}
