package org.es.unir.backend.securityconcept.reports.domain.value_objects;

public enum Status {
    PENDING, ASSIGNED, CANCELLED, FAILED, REJECTED
}
