package org.es.unir.backend.securityconcept.users.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.utils.JWT;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping ("/api/v1/users")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class AuthUserController {
    @GetMapping ("/auth")
    public ResponseEntity<AuthUserResponse> getUser(
            @AuthenticationPrincipal Jwt jwt)
    {
        return ResponseEntity.ok()
                             .body(new AuthUserResponse(JWT.getUserNameOf(jwt)));
    }

    record AuthUserResponse(String name) {}
}
