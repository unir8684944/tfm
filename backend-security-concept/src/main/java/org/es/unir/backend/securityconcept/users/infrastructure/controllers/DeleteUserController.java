package org.es.unir.backend.securityconcept.users.infrastructure.controllers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.es.unir.backend.securityconcept.users.application.UserDeleteUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping ("/api/v1/users")
@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
public class DeleteUserController {
    UserDeleteUseCase userDeleteUseCase;

    @DeleteMapping ("/delete/{email}")
    public ResponseEntity<String> createUser(
            @PathVariable String email)
    {
        return userDeleteUseCase.deleteCustomer(email)
                                .isRight() ? ResponseEntity.ok()
                                                           .build()
                                           : ResponseEntity.badRequest()
                                                           .body("no created");
    }
}
