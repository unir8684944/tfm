# Proyecto TFM

Este proyecto se estructura en varias carpetas y archivos que incluyen conceptos de seguridad para el backend y el frontend. A continuación, se detallan los componentes principales del proyecto, su arquitectura, y las medidas de seguridad implementadas.

## Estructura del Proyecto

- `infra/`: Contiene la infraestructura del proyecto.

  - `back/`: Configuraciones y scripts relacionados con el backend.
  - `db/`: Configuraciones y scripts relacionados con la base de datos.

- `backend-security-concept/`: Conceptos y configuraciones de seguridad para el backend.

  - `docker-compose.yml`: Configuración de Docker para el entorno de desarrollo.
  - `pom.xml`: Archivo de configuración de Maven para la gestión de dependencias.
  - `src/`: Código fuente del backend.

- `frontend-secure-concept/`: Conceptos y configuraciones de seguridad para el frontend.

  - `Dockerfile`: Configuración de Docker para el frontend.
  - `.env.production`: Variables de entorno para el entorno de producción.
  - `nginx.conf`: Configuración de Nginx para servir la aplicación frontend.

- `doc/`: Documentación del proyecto.

  - `imgs/`: Imágenes utilizadas en la documentación.
  - `diagrams/`: Diagramas explicativos del proyecto.
  - `examples/`: Fragmentos de código ilustrativos del proyecto.

- `.gitlab-ci.yml`: Configuración de CI/CD para GitLab.

- `qodana.yaml`: Configuración de Qodana para análisis de código.

## Arquitectura del Proyecto

### Backend

El backend del proyecto se basa en una arquitectura de microservicios, donde cada servicio es responsable de una funcionalidad específica. Los servicios están contenedorizados usando Docker para asegurar un entorno consistente en desarrollo y producción. La comunicación entre los servicios se realiza a través de APIs RESTful.

Componentes principales:

- **Servicio de Autenticación**: Gestiona la autenticación y autorización de usuarios.
- **Servicio de Datos**: Maneja las operaciones CRUD sobre la base de datos.
- **Servicio de Notificaciones**: Envía notificaciones a los usuarios a través de distintos canales.

### Frontend

El frontend está construido usando un framework moderno como React o Vue.js, y está configurado para ser servido por un servidor Nginx. El uso de Docker asegura que la aplicación frontend se ejecute en un entorno controlado, eliminando problemas de configuración entre distintos entornos.

Componentes principales:

- **Interfaz de Usuario (UI)**: Desarrollada con componentes reutilizables para asegurar consistencia y facilitar el mantenimiento.
- **Gestión de Estado**: Uso de una biblioteca de gestión de estado para manejar el estado global de la aplicación.
- **Comunicación con Backend**: Las llamadas a las APIs del backend se gestionan de manera segura usando tokens de autenticación.

### Infraestructura

La infraestructura del proyecto está definida en archivos de configuración que permiten la orquestación y despliegue de los servicios. Se utilizan herramientas como Docker Compose para definir y gestionar el entorno de desarrollo.

## Medidas de Seguridad

### Backend

1.  **Configuración de Dependencias**: Uso de `pom.xml` para gestionar las dependencias, asegurando que todas las bibliotecas sean actualizadas y seguras.
    - **Dependencias Seguras**: Solo se incluyen dependencias de repositorios confiables y se utilizan versiones específicas para evitar vulnerabilidades conocidas.
2.  **Docker**: Uso de `docker-compose.yml` para aislar el entorno de desarrollo y producción, evitando conflictos y mejorando la seguridad.
    - **Aislamiento de Servicios**: Cada servicio se ejecuta en su propio contenedor, limitando el alcance de posibles vulnerabilidades.
3.  **Control de Código**: `.gitignore` para excluir archivos sensibles del control de versiones.
    - **Gestión de Credenciales**: Las credenciales y secretos no se almacenan en el repositorio de código, sino que se gestionan a través de variables de entorno.
4.  **Validación de Entradas**: Implementación de validaciones en el código fuente para prevenir inyecciones SQL y otras vulnerabilidades.
    - **Sanitización de Datos**: Todos los datos de entrada se sanitizan antes de ser procesados para evitar ataques de inyección.
5.  **Autenticación y Autorización**: Uso de mecanismos seguros para la autenticación y autorización de usuarios.
    - **Tokens JWT**: Uso de tokens JWT (JSON Web Tokens) para la autenticación de usuarios, asegurando que las sesiones sean seguras y fáciles de manejar.

### Frontend

1.  **Variables de Entorno**: Uso de `.env.production` para gestionar las configuraciones sensibles.
    - **Separación de Entornos**: Diferentes archivos de configuración para entornos de desarrollo y producción, asegurando que las configuraciones sensibles no se expongan.
2.  **Nginx**: Configuración segura en `nginx.conf` para servir la aplicación de manera eficiente y segura.
    - **CORS**: Configuración de CORS para restringir el acceso a las APIs solo desde dominios permitidos.
3.  **Docker**: Uso de Docker para contenerizar la aplicación, asegurando que se ejecute en un entorno controlado.
    - **Imágenes Optimizadas**: Las imágenes de Docker se construyen siguiendo buenas prácticas para minimizar el tamaño y mejorar la seguridad.
4.  **Análisis de Código**: Integración con Qodana (`qodana.yaml`) para realizar análisis estático de código y detectar posibles vulnerabilidades.
    - **Linting y Formateo**: Uso de herramientas de linting y formateo para mantener un código limpio y libre de errores comunes.

### Documentación

- **Diagrams y Documentos**: Documentación detallada en `doc/` para mantener un registro claro de la arquitectura y las decisiones de diseño del proyecto.
  - **Diagramas de Arquitectura**: Diagramas que explican la arquitectura de los componentes y su interacción.
  - **Manuales de Usuario**: Guías y manuales para ayudar a los desarrolladores y usuarios a entender y utilizar el sistema de manera efectiva.

## Instrucciones de Configuración

### Backend

1.  Clonar el repositorio.
2.  Navegar a la carpeta `backend-security-concept/`.
3.  Ejecutar `mvn clean install` para instalar las dependencias.
4.  Configurar las variables de entorno necesarias.
5.  Ejecutar `docker-compose up` para iniciar los servicios.

### Frontend

1.  Clonar el repositorio.
2.  Navegar a la carpeta `frontend-secure-concept/`.
3.  Ejecutar `npm install` para instalar las dependencias.
4.  Configurar las variables de entorno en `.env.production`.
5.  Ejecutar `docker build -t frontend .` y luego `docker run -p 80:80 frontend`.

## Contribución

1.  Hacer un fork del repositorio.
2.  Crear una rama para la nueva funcionalidad (`git checkout -b feature/nueva-funcionalidad`).
3.  Realizar los cambios y hacer commit (`git commit -m 'Agregar nueva funcionalidad'`).
4.  Hacer push a la rama (`git push origin feature/nueva-funcionalidad`).
5.  Crear un Pull Request.
