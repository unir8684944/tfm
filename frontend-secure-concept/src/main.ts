import {createApp} from 'vue'
import App from './App.vue'
import keycloak from './utilities/keycloak.ts';
import router from "./rutes/router.ts";
import './index.css'


keycloak.init({
    onLoad: 'login-required',
    enableLogging: true,
    checkLoginIframe: false,
    flow: 'standard'
}).then((authenticated: boolean) => {
    if (authenticated) {
        createApp(App).use(router).mount('#app');
    } else {
        window.location.reload();
    }
}).catch(() => {
    //console.error('Authentication Failed' + err);
}).finally(() => {

});