import Keycloak from 'keycloak-js';
import {url_auth} from "./varMode.ts";

const keycloak = new Keycloak({
    url: url_auth,
    realm: 'security-concept',
    clientId: 'security-concept',
});

export default keycloak;
