import {requestwithPath} from "../RequestwithPath.ts";

const hookGetMenu = async () => {
    const res =
        await requestwithPath("/api/v1/users/menu")
            .withMethode("GET")
            .withBody(null)
    return await res?.resp;
}

export default hookGetMenu;