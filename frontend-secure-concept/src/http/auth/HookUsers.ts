import {requestwithPath} from "../RequestwithPath.ts";

type USER_AUTH = {
    name: string | null
}
const hookGetUserInAuth = async () => {
    const res: USER_AUTH = await requestwithPath("/api/v1/users/auth")
        .withMethode("GET")
        .withBody(null)
        .then(res => res?.resp)
    return res;
}

export default hookGetUserInAuth