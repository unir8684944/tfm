import {requestwithPath} from "../RequestwithPath.ts";

export interface Roles {
    name: string,
    uuid: string
}

[]

export type USER_TYPE = {
    "uuid": string,
    "name": string,
    "firstName": string,
    "email": string,
    "username": string,
    "authorities": Roles,
    "roles": Roles,
    "enabled": boolean,
    "credentialsNonExpired": boolean,
    "accountNonExpired": boolean,
    "accountNonLocked": boolean
};

const hookGetAllUsers = async () => {

    const res: USER_TYPE[] = await requestwithPath("/api/v1/users?role=")
        .withMethode("GET")
        .withBody(null)
        .then(res => res?.resp)
    return res;
}

export default hookGetAllUsers