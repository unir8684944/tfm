import {requestwithPath} from "../RequestwithPath.ts";
import {USER_TYPE} from "./HookGetAllUsers.ts";

const hookGetUserByRole = async () => {

    const res: USER_TYPE[] = await requestwithPath("/api/v1/users?role=support")
        .withMethode("GET")
        .withBody(null)
        .then(value => value?.resp)
    return res;
}

export default hookGetUserByRole;