import keycloak from "../utilities/keycloak.ts";
import {url_back} from "../utilities/varMode.ts";

const MET = ['GET', 'POST', 'DELETE'] as const;
type METType = typeof MET[number];

export const requestwithPath = (path: string) => {

    return {
        withMethode: (methode: METType) => {
            return {
                withBody: async (body: unknown | null | undefined) => {
                    const resp: Response = await fetch(url_back + path, {
                        method: methode,
                        headers: {
                            "Authorization": `Bearer ${keycloak.token}`,
                            "Content-Type": "application/json",
                            "Accept": "application/json",
                        },
                        body: body ? JSON.stringify(body) : null,
                    })
                    if (!resp.ok) {
                        //console.log("Request Error", resp.status)
                        return;
                    }
                    return {
                        resp: resp.json()
                    }
                }
            }
        }

    }

}