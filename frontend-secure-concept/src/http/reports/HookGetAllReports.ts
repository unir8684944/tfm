import {requestwithPath} from "../RequestwithPath.ts";

export type REPORT_TYPE = {
    "id": string,
    "title": string,
    "description": string,
    "status": string,
    "reporter": string,
    "support":  string | null
};

const hookGetAllReports = async () => {
    const res: REPORT_TYPE[] = await requestwithPath("/api/v1/reports")
        .withMethode("GET")
        .withBody(null)
        .then(res => res?.resp)
    return res;
}

export default hookGetAllReports