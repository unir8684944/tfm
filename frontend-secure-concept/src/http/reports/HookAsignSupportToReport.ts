import {requestwithPath} from "../RequestwithPath.ts";

type EmailSupoer = { "supporterId": string }

const hookAsignSupportToReport = async (id: string, emailSupport: EmailSupoer) => {
    const res = await requestwithPath(`/api/v1/reports/${id}`)
        .withMethode("POST")
        .withBody(emailSupport)
        .then(res => res?.resp);
    return res
}
export default hookAsignSupportToReport;