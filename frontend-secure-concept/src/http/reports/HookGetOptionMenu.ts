import {requestwithPath} from "../RequestwithPath.ts";

const hookGetMenu = async (): Promise<string[]> => {
    const res =
        await requestwithPath("/api/v1/reports/select-option")
            .withMethode("GET")
            .withBody(null)
    return await res?.resp;
}

export default hookGetMenu;