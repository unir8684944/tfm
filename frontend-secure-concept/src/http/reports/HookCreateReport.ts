import {requestwithPath} from "../RequestwithPath.ts";

type REPORT_TYPE = {
    title: string,
    description: string,
}
const hookCreateReport = async (report: REPORT_TYPE) => {
    const res = await requestwithPath("/api/v1/reports")
        .withMethode("POST").withBody(report)
        .then(res => res?.resp);
    return res;
}

export default hookCreateReport