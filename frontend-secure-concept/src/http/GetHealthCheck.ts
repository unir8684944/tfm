import {requestwithPath} from "./RequestwithPath.ts";

type Res = { status: string };

const hookHealtCheck = async () => {
    const res: Res = await requestwithPath("/healthCheck")
        .withMethode("GET")
        .withBody(null)
        .then(res => res?.resp)
    return res;
}

export default hookHealtCheck