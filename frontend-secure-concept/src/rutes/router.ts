import {createRouter, createWebHistory} from 'vue-router'

import HomeView from '../view/home/Home.vue'
import Users from "../view/users/Users.vue";
import Report from "../view/reports/Report.vue";
import ErrorView from "../view/error/ErrorView.vue";
import keycloak from "../utilities/keycloak.ts";

const routes = [
    {path: '/', component: HomeView},
    {path: '/home', name:'home', component: HomeView},
    {path: '/reports', name:'reports', component: Report},
    {path: '/users', name:'users', component: Users},
    {path: '/:pathMatch(.*)*', name:'error', component: ErrorView}
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

router.beforeEach((to, _from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!keycloak.authenticated) {
            keycloak.login();
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;